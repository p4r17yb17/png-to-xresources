# PNG to Xresources

PNG to Xresources is a simple shell script which automatically applies the color range of a PNG to a colorscheme for urxvt.

> **Note**
> * PNG to Xresources consists of multiple programs which can be found in the bin directory.
> * Once you build hex2col and colors, move them into the directory of png2Colorscheme.sh and urxvt.sh or change the script.


## Compiling
### Hex2col

Hex2col displays the colors of received HTML color codes

To build hex2col execute: <br>
`gcc hex2col.c -o hex2col`


### Colors

A simple program to extract colors from PNG files.  It is similar to strings but for pictures.

To build colors execute: <br>
`make`


## Usage

To use the png2Colorscheme you'll need to run the script and pass the image you want to extract the colour from as an argument. Like this: <br>
`./png2Colorscheme.sh choice.png`
